from flask import Flask, jsonify, request, abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)

@app.route('/meeting')
def get_meetings():
    return jsonify(data['meeting'])

@app.route('/meeting/<string:id>')
def get_meeting_info(id):
    return jsonify(data['meeting'][id])

@app.route('/instructors')
def get_instructors():
    return jsonify(data['instructors'])

@app.route('/instructors/<int:id>')
def get_instructor_by_id(id):
    if len(data['instructors'])>id:
        return jsonify(data['instructors'][id])
    else:
        return abort(404)

@app.route('/instructors/<int:id>/<string:field>')
def get_instructor_name(id, field):
    return jsonify(data['instructors'][id][field])


@app.route('/assignments')
def get_assignment():
    return jsonify(data['assignments'])

@app.route('/assignments/<int:id>')
def get_assignment_by_id(id):
    return jsonify(data['assignments'][id])



@app.route('/assignments/<int:id>/<string:field>')
def get_assignment_name(id, field):
        return jsonify(data['assignments'][id][field])

@app.route('/assignments',methods=['POST'])
def post_assignment():
    request_data = request.data.decode("utf-8")
    request_dictionary = json.loads(request_data)
    return jsonify(data['assignments'].append(request_dictionary))
